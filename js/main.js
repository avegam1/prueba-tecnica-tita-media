document.querySelector('.boton').addEventListener('click',()=>{
	document.querySelector('.nav-menu').classList.toggle('show');

});

const grid=  new Muuri('.grid', {
	layout: {
		rounding: false
	}
});
window.addEventListener('load',()=>{
	grid.refreshItems().layout();
	document.getElementById('grid').classList.add('img-loads');
	const enlaces = document.querySelectorAll('#categorias li  a');
	enlaces.forEach((elemento) => {
		elemento.addEventListener('click', (evento) => {
			evento.preventDefault();
			enlaces.forEach((enlace) => enlace.classList.remove('activo'));
			evento.target.classList.add('activo');

			const categoria = evento.target.innerHTML.toLowerCase();
			categoria === 'all' ? grid.filter('[data-categoria]') : grid.filter(`[data-categoria="${categoria}"]`);
		});
	});
});
